package edu.unk.fun330;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

/**
 * Parent class of all game FlyingObjects. Bonus and Ship are subclasses.
 * 
 * @author hastings
 *
 */
public abstract class FlyingObject {

	protected float facing;
	protected float radius;

	protected float heading;
	protected float speed;
	protected float acceleration;

	protected float x;
	protected float y;

	public FlyingObject(float x, float y) {
		this.x = x;
		this.y = y;

		facing = 0;
		heading = 0;
		speed = 0;
		radius = 0;
	}

	/** 
	 * Have the object react to being hit by another flying object.
	 * Return true if the object should be removed from the universe after being hit.
	 * 
	 * @param fo - flying objecy
	 * @param u - game universe
	 * @return true if the object should be removed due to collision
	 */
	protected abstract boolean hitBy(FlyingObject fo, Universe u);
	
	protected abstract Color getColor();
	
	/**
	 * Return the radius of the flying object.
	 */
	public abstract float getRadius();
	
	/**
	 * Return the loaded image for the game object.
	 */
	protected Image getImage() {return null;};
	
	protected void setRadius(float r) {this.radius = r; }
	
	/**
	 * Paint the flying object.
	 * @param g - graphics "pen"
	 */
	public abstract void paint(Graphics g);

	/** @return the facing */
	public float getFacing() { return facing; }
	/** @return the heading */
	public float getHeading() { return heading; }
	/** @return the speed */
	public float getSpeed() { return speed; }
	/** @return the x coordinate of the location */
	public float getX() { return x; }
	/** @return the y coordinate of the location */
	public float getY() { return y; }
	protected void setFacing(float facing) { this.facing = facing; }
	protected void setHeading(float heading) { this.heading = heading; }
	protected void setSpeed(float speed) { this.speed = speed; }
	protected void setX(float x) { this.x = x; }
	protected void setY(float y) { this.y = y; }
	/** @return the x coordinate at the next frame based on the current heading */
	public float getNextX() { return  x + speed * (float) Math.cos(heading); }
	/** @return the y coordinate at the next frame based on the current heading */
	public float getNextY() { return  y + speed * (float) Math.sin(heading); }
	/** @return the estimated x coordinate in <b>i</b> frames based on the current heading */
	public float getNextX(int i) { return  x + speed * i * (float) Math.cos(heading); }
	/** @return the estimated y coordinate in <b>i</b> frames based on the current heading */
	public float getNextY(int i) { return  y + speed * i * (float) Math.sin(heading); }
	/** @return the acceleration */
	public float getAcceleration() { return acceleration; }
	protected void setAcceleration(float acceleration) { this.acceleration = acceleration; }
	
	protected boolean offLeftUniverse() { return (x-radius < 0); }
	protected boolean offRightUniverse() { return (x+radius> Universe.WIDTH); }
	protected boolean offBottomUniverse() { return (y-radius<0); }
	protected boolean offTopUniverse() { return (y+radius>Universe.HEIGHT); }
	protected boolean offUniverse() {
		return (offLeftUniverse() || offRightUniverse() || offBottomUniverse() || offTopUniverse());
	}
	

	/**
	 * Return true if the object should be removed for leaving the universe.
	*/
	protected abstract boolean handleOffUniverse(Universe gd);
	
	/**
	 * Return the distance to the flying object.
	 */
	private float distance (FlyingObject fo) {
		return Util.distance(this.getNextX(), this.getNextY(), fo.getNextX(), fo.getNextY() );
	}

	/**
	 * Return true if the object intersects the flying object.
	 */
	protected boolean intersects (FlyingObject fo) {
		float distance = this.distance(fo);
		//If there is a collision
		return (distance < this.getRadius() + fo.getRadius());
	}

}
