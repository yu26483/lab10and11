package edu.unk.fun330.controllers;

import edu.unk.fun330.*;
import edu.unk.fun330.base.*;

/** 
 * Example of a very basic ship controller which targets the nearest ship
 * and fires a bullet every 30 frames (until out of rounds).  The controller also
 * alternates between flying and stopped every 100 frames.  Notice that a ship can
 * stop on a dime, reorient it's direction and start moving again after 100 frames.
 * Note that this controller does not lead the moving targets.
 * 
 * <p>
 * Declare instance variables as needed for your controller.
 * 
 * <p>A possible approach to implementing a controller would be as a state machine:
 * https://www.baeldung.com/java-enum-simple-state-machine
 * https://cleanjava.wordpress.com/2012/02/25/enum-as-state-machine/
 * 
 * @author John Hastings
 **/
public class YourShipController3 extends ShipController{

	public YourShipController3(Ship ship) {
		super(ship);
	}

	@Override
	public String getName() { return "Tyler1"; }
	private boolean stop = false;
	private int i = 0;  // frame counter used only for this stupid controller, resets after 1000 frames
	
	private BombBonus bombbonus;
	private BulletBonus bulletbonus;
	private EmpBonus empbonus;
	private FuelBonus fuelbonus;
	private LaserBonus lasterbonus;
	private MagnetBonus magnetbonus;
	private PointsBonus pointsbonus;
	private ShieldBonus shieldbonus;
	private ShipJumpBonus shipjumpbonus;
	
	
	
	/**
	 * Called by the engine to get the next move of the
	 * ship/controller. This method can call loop through the flying objects and to determine an 
	 * action to return.
	 * 
	 * <p>This example controller fires a bullet every 30 frames, and alternates 
	 * between flying toward a target and stopping, and making a flight adjustment.  Note that only one
	 * action can be returned each time makeMove is called.  So, you might need to use status variables to keep track of what was previously done,
	 * and what remains to be done.
	 */
	@Override
	public ControllerAction makeMove(Universe u) {
				
		
		FlightAdjustment fa = new FlightAdjustment();

		FlyingObject closestObject = null;
		float closestObjectDistance = Float.MAX_VALUE;

		// loop through all flying objects
		// find the closest ship.  This controller is ignoring bonuses.
		for (FlyingObject flyingObject : u.getFlyingObjects()) {
			
			// this controller is only looks at other ships and ignores everything else
			if(ship.equals(flyingObject))          //skip my own ship    //only look for other ships
				continue;
			
			float distance = Util.distance (ship.getX(),ship.getY(),flyingObject.getX(),flyingObject.getY() );
			
			  
				if(flyingObject instanceof FuelBonus) {
					float angle = Util.calcAngle(ship.getX(), ship.getY(), 
						flyingObject.getX(), flyingObject.getY());
				fa.setFacing(angle);
				fa.setAcceleration(0.8f);
				}
				else if(flyingObject instanceof Ship) {
					float angle = Util.calcAngle(ship.getX(), ship.getY(), 
						flyingObject.getX(), flyingObject.getY());
				fa.setFacing(angle);
				i = (i+1)%1000;
				
				if(i%3 == 0){ return new FireLaser(); }
			
				}else if(distance < closestObjectDistance){
				
					if(flyingObject instanceof Bonus) {
					if(flyingObject instanceof BombBonus || flyingObject instanceof BlackHole) {
						
					}else {
						fa.setAcceleration(30f);
					closestObjectDistance = distance;
					closestObject = flyingObject; //enemy
					}
				}
				//closestObjectDistance = distance;
				//closestObject = flyingObject; //enemy
				
	
			}	

		}
		
		// if 100 frames have passed, switch from flying to stopped or vice versa
		if (ship.getAcceleration() > 0) { fa.setAcceleration(0);}

		if(closestObject instanceof BlackHole) {
			float facing = Util.calcAngle(ship.getX(), ship.getY(), closestObject.getX(), closestObject.getY());
			fa.setFacing(facing - Util.PI / 1f);
			return fa;
		}
		// only update the facing if closestObject is not null
		// should lead the ship based on expected future location
		if (closestObject == null ) {
			return fa;
		}
			// set the facing toward the closest object
			float angle = Util.calcAngle(ship.getX(), ship.getY(), 
					closestObject.getX(), closestObject.getY());
			fa.setFacing(angle);
		
		
		return fa;
	}
	
}