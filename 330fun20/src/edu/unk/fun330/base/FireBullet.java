package edu.unk.fun330.base;

/**
 * Fire bullet action returned by controllers from their makeMove method.
 * Works only when the ship canFire method returns true.
 * 
 * @author hastings
 *
 */
public class FireBullet extends ControllerAction { }
