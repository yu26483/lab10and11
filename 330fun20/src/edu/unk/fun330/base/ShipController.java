package edu.unk.fun330.base;

import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;

/**
 * Parent class for all ship controllers.
 * 
 * @author hastings
 *
 */
public abstract class ShipController {
	
	protected final Ship ship;
	
	public ShipController(Ship ship){ this.ship = ship; }

	public abstract String getName();
	
	/** Called by the engine to get the next move of the ship/controller. This method 
	 * can call loop through the flying objects and to determine an action to return.
	 * 
	 * <b>Note</b>: Perhaps this should be modified to not pass the entire universe into the 
	 * controllers and only pass the game objects!!!
	 * 
	 * @param gd - game universe
	 * @return controller action
	 */
	public abstract ControllerAction makeMove(Universe gd);

	/** @return the ship tied to this controller */
	public Ship getShip() { return ship; }
}
